﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Generate a screenshot and save to disk with the name SomeLevel.png.

public class Screenshot : MonoBehaviour
{   public GameObject Canvas1;
    public void OnClickScreenCaptureButton()
 {
    StartCoroutine(CaptureScreen());
 }
    public IEnumerator CaptureScreen()
    {
        // Wait till the last possible moment before screen rendering to hide the UI
        yield return null;
        Canvas1.SetActive(false);

        // Wait for screen rendering to complete.
        yield return new WaitForEndOfFrame();

        // Take screenshot
        Texture2D screenImage = new Texture2D(Screen.width, Screen.height);
        //Get Image from screen
        screenImage.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        screenImage.Apply();
        //Convert to png
        byte[] imageBytes = screenImage.EncodeToPNG();

        //Save image to gallery
        NativeGallery.SaveImageToGallery(imageBytes, "AlbumName", "album.png", null);
        AndroidNativeFunctions.ShowToast("Photo Saved to Gallery");

        // Show UI after we're done
         Canvas1.SetActive(true);
    } 
}
